-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 18-Jun-2015 às 16:59
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wr_construtora`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `despesa`
--

CREATE TABLE IF NOT EXISTS `despesa` (
`id` int(11) NOT NULL,
  `descricao` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `idProjeto` int(11) NOT NULL,
  `idUsuarioCadastro` int(11) NOT NULL,
  `dataDoCadastro` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `despesa`
--

INSERT INTO `despesa` (`id`, `descricao`, `valor`, `idProjeto`, `idUsuarioCadastro`, `dataDoCadastro`) VALUES
(8, 'dasdsadsadsa', 1, 25, 2, '2015-06-18 16:58:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `despesa_removida`
--

CREATE TABLE IF NOT EXISTS `despesa_removida` (
`id` int(11) NOT NULL,
  `descricao` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `idProjeto` int(11) NOT NULL,
  `idUsuarioRemoveu` int(11) NOT NULL,
  `dataDoCadastro` datetime NOT NULL,
  `dataDaRemocao` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `despesa_removida`
--

INSERT INTO `despesa_removida` (`id`, `descricao`, `valor`, `idProjeto`, `idUsuarioRemoveu`, `dataDoCadastro`, `dataDaRemocao`) VALUES
(2, 'Désddsú', 200, 24, 2, '2015-06-18 15:24:10', '2015-06-18 15:25:09'),
(3, 'sadsadsa', 3, 24, 2, '2015-06-18 15:48:41', '2015-06-18 16:02:05'),
(4, 'Dsadsadsadsadsa', 1500.8, 25, 2, '2015-06-18 16:03:54', '2015-06-18 16:39:04'),
(5, 'sdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 3, 25, 2, '2015-06-18 16:38:54', '2015-06-18 16:39:26'),
(6, 'Treze', 13, 25, 2, '2015-06-18 16:33:24', '2015-06-18 16:39:36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto`
--

CREATE TABLE IF NOT EXISTS `projeto` (
`id` int(11) NOT NULL,
  `nome` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `dataDoCadastro` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `idUsuarioCadastro` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto`
--

INSERT INTO `projeto` (`id`, `nome`, `descricao`, `dataDoCadastro`, `status`, `idUsuarioCadastro`) VALUES
(25, 'Téste dos Téstes', '', '2015-06-18 16:03:37', 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `login` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `dataDoCadastro` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `login`, `senha`, `dataDoCadastro`, `status`) VALUES
(1, 'Administrador', 'admin', 'huehue', '2015-06-18 14:51:51', 1),
(2, 'Usuário 2', 'usuario', 'admin', '2015-06-18 14:53:07', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `despesa`
--
ALTER TABLE `despesa`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `despesa_removida`
--
ALTER TABLE `despesa_removida`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projeto`
--
ALTER TABLE `projeto`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `despesa`
--
ALTER TABLE `despesa`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `despesa_removida`
--
ALTER TABLE `despesa_removida`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `projeto`
--
ALTER TABLE `projeto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
