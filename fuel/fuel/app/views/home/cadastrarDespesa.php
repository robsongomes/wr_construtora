<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?= (isset($pageTitle) && !empty($pageTitle)) ? $pageTitle : 'WR Construtora' ?></title>

        <!--Importando a fonte-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:700,100" rel="stylesheet" type="text/css">

        <!--Importando font-awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <!--importando boottrap-->
        <?php echo Asset::css('bootstrap.min.css'); ?>
        <?php echo Asset::css('bootstrap-theme.min.css'); ?>       

        <!--Animate.css -->        
        <?php echo Asset::css('animate.css'); ?>    

    </head>
    <body>    
        <?php if (Session::get('message') != null): ?>            
            <div class="alert alert-<?php echo Session::get('message')->type ?>"><?php echo Session::get('message')->body ?></div>
            <?php Session::delete('message'); ?>
        <?php endif; ?>        

        <!--Importando javascript-->
        <?php echo Asset::js('jquery.min.js'); ?>
        <?php echo Asset::js('bootstrap.min.js'); ?>   


        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
                <h3 class="titulo"><?= (isset($despesa) && !empty($despesa)) ? 'Despesa "' . Controller_Logged::cutText($despesa->descricao, 22) . '"' : 'Nova Despesa' ?></h3>
                <hr>
                <form id="formCadastrar" name="formCadastrar" method="post" action="/public/home/<?= (isset($despesa) && !empty($despesa)) ? "editarDespesa/$despesa->id/$idProjeto" : "cadastrarDespesa/$idProjeto" ?>">            
                    <div class="form-group">
                        <label for="descricao">Descrição*</label>
                        <input type="text" maxlength="500" class="form-control" id="descricao" name="descricao" value="<?= (isset($despesa->descricao) && !empty($despesa->descricao)) ? $despesa->descricao : '' ?>" placeholder="Digite a descrição" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="valor">Valor*</label>
                        <input type="number" step="any" min="1" class="form-control" id="valor" name="valor" placeholder="Digite o valor da despesa" value="<?= (isset($despesa->valor) && !empty($despesa->valor)) ? $despesa->valor : '' ?>" required>
                    </div>                                  
                    <hr>             
                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Salvar</button>
                    <a href="/public/home/projeto/<?= $idProjeto ?>"  class="btn btn-danger pull-right" style="margin-right: 8px;"><i class="fa fa-ban"></i> Cancelar</a>
                </form>  
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.tooltype').tooltip();
                $('.alert alert-danger alert-dismissible').add("button");  //função para criar caixas de texto ao passar o cursor do mouse.

                /* essas são responsáveis por fazer as mensagens flash desaparecerem suavemente após
                 * determinado período de tempo.
                 */
                $(".alert-success").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-success").alert('close');
                });
                $(".alert-danger").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
                $(".alert-warning").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
                $('.alert').addClass('animated fadeIn');
            });
        </script>          
    </body>
</html>
