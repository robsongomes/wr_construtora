<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?= (isset($pageTitle) && !empty($pageTitle)) ? $pageTitle : 'WR Construtora' ?></title>

        <!--Importando a fonte-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:700,100" rel="stylesheet" type="text/css">

        <!--Importando font-awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <!--importando boottrap-->
        <?php echo Asset::css('bootstrap.min.css'); ?>
        <?php echo Asset::css('bootstrap-theme.min.css'); ?>       

        <!--Animate.css -->        
        <?php echo Asset::css('animate.css'); ?>

        <!--Importando estilo do template-->        
        <?php echo Asset::css('template.css'); ?>

        <style>
            #nomeCompletoProjeto {
                cursor: pointer;
            }    
        </style>

    </head>
    <body>    
        <?php if (Session::get('message') != null): ?>            
            <div class="alert alert-<?php echo Session::get('message')->type ?>"><?php echo Session::get('message')->body ?></div>
            <?php Session::delete('message'); ?>
        <?php endif; ?>                    

        <!--Importando javascript-->
        <?php echo Asset::js('jquery.min.js'); ?>
        <?php echo Asset::js('bootstrap.min.js'); ?>   


        <nav id="navbarPrincipal" class="navbar navbar-default navbar-static">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/public/home/index">WR Const.</a>
                </div>
                <div class="collapse navbar-collapse bs-example-js-navbar-collapse">          
                    <ul class="nav navbar-nav navbar-right hidden-xs hidden-sm">
                        <li id="fat-menu" class="dropdown">
                            <a id="drop3" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                <i class="fa fa-bars fa-lg"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="/public/home/cadastrarDespesa/<?= $projeto->id ?>"><i class="fa fa-plus"></i> Nova Despesa</a></li>                                        
                                <li class="removerDespesa" role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-lock lock-despesa"></i> Gerenciar Despesa</a></li>                                    
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right hidden-md hidden-lg">                                    
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/public/home/cadastrarDespesa/<?= $projeto->id ?>"><i class="fa fa-plus"></i> Nova Despesa</a></li>
                        <li class="removerDespesa" role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-lock lock-despesa"></i> Gerenciar Despesa</a></li>                                           
                    </ul>
                </div><!-- /.nav-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="container">       
            <div class="row table-dados">   
                <div class="titulo">            
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <h4 id="nomeCompletoProjeto" nomeProjeto="<?= (isset($projeto->nome) && !empty($projeto->nome)) ? $projeto->nome : '--' ?>" descricaoProjeto="<?= (isset($projeto->descricao) && !empty($projeto->descricao)) ? $projeto->descricao : '' ?>"><?= (isset($projeto->nome) && !empty($projeto->nome)) ? Controller_Logged::cutText($projeto->nome, 20) : '--' ?></h4>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding: 0;">
                        <span class="label label-primary pull-right" style="font-size: 16px;border-radius: 0;"><?= (isset($valorTotal) && !empty($valorTotal)) ? 'R$ ' . number_format($valorTotal, 2, ',', '.') : 'R$ 0' ?></span>
                    </div>            
                </div>                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0; margin: 0;">
                    <table id="tableDespesa" class="table table-hover">
                        <thead>                    
                            <tr id="removerAtivoInativo"  style="background-color: #E2E2E2;transition: all 0.2s linear;">                        
                                <th style="width: 65%;text-align: center;">Descrição</th>
                                <th style="width: 30%;text-align: center;">Valor</th>                                                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($despesas) && !empty($despesas)): ?>
                                <?php foreach ($despesas as $key => $d): ?>                            
                                    <tr id="<?= $d->id ?>" value="<?= $projeto->id ?>" nomeDespesa="<?= (isset($d->descricao) && !empty($d->descricao)) ? $d->descricao : '--' ?>" dataDespesa="<?= (isset($d->dataDoCadastro) && !empty($d->dataDoCadastro)) ? date("d/m/Y", strtotime($d->dataDoCadastro)) : '--' ?>" class="removerItem" style="text-align: center;">                                
                                        <td><?= (isset($d->descricao) && !empty($d->descricao)) ? Controller_Logged::cutText($d->descricao, 25) : '--' ?></td>
                                        <td><?= (isset($d->valor) && !empty($d->valor)) ? 'R$ ' . number_format($d->valor, 2, ',', '.') : '--' ?></td>                                                                                                                                
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="2">Não há nenhuma despesa cadastrada ainda</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>  
            </div>      
        </div>

        <!--Modal confirmação de remoção de despesa -->
        <div class="row">
            <div class="modal fade col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="row">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button"  id="closeInfo" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h5 class="modal-title" id="myModalLabel">Despesa <span id="textoDespesaData" class="label label-warning"></span></h5>
                            </div>

                            <div class="modal-body" id="divExcluir">
                                <p id="textoNomeDespesa">Tem certeza que deseja excluir/alterar esta despesa?</p>
                            </div>         
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
                                <a href="#"  id="btnExcluir" class="btn btn-default btn-xs"><i class="fa fa-trash"></i> Excluir</a>
                                <a href="#"  id="btnEditar" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> Editar</a>
                            </div>            
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>    
            </div>
        </div>    

        <!--Modal para exibir nome completo do projeto e descrição(se houver) -->
        <div class="row">
            <div class="modal fade col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2" id="modalProjeto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="row">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                        
                            </div>

                            <div class="modal-body">
                                <p class="modal-title" id="modalNomeCompletoProjeto"></p>
                                <p id="modalDescricaoProjeto"></p>
                            </div>         
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Fechar</button>                        
                            </div>            
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>    
            </div>
        </div>    
        <script type="text/javascript">
            $(document).ready(function() {
                var color = true;
                $('.removerDespesa').click(function() {
                    if (color) {
                        $('.removerItem').addClass('deleteItem');
                        $('#removerAtivoInativo').css("background-color", "#faebcc");
                        $('.lock-despesa').removeClass('fa-lock');
                        $('.lock-despesa').addClass('fa-unlock').hide().fadeIn('slow');
                        color = false;
                    } else {
                        $('.removerItem').removeClass('deleteItem');
                        $('#removerAtivoInativo').css("background-color", "#fff");
                        $('.lock-despesa').removeClass('fa-unlock');
                        $('.lock-despesa').addClass('fa-lock').hide().fadeIn('slow');
                        color = true;
                    }
                });

                $("#tableDespesa").on("click", '.deleteItem', function() {
                    var idDespesa = $(this).attr('id');
                    var idProjeto = $(this).attr('value');
                    var dataDespesa = $(this).attr('dataDespesa');
                    var nomeDespesa = $(this).attr('nomeDespesa');
                    $('#textoDespesaData').text(dataDespesa);
                    $('#textoNomeDespesa').text('Tem certeza que deseja excluir/alterar a despesa "' + nomeDespesa + '"?');
                    $('#btnExcluir').attr('href', '/public/home/deletarDespesa/' + idDespesa + '/' + idProjeto);
                    $('#btnEditar').attr('href', '/public/home/editarDespesa/' + idDespesa + '/' + idProjeto);
                    $('#modalExcluir').modal('show');
                });

                $('#nomeCompletoProjeto').click(function() {
                    var nomeProjeto = $(this).attr('nomeProjeto');
                    var descricaoProjeto = $(this).attr('descricaoProjeto');
                    $('#modalNomeCompletoProjeto').text('Projeto: ' + nomeProjeto);
                    $('#modalDescricaoProjeto').text('Descrição: ' + descricaoProjeto);
                    $('#modalProjeto').modal('show');
                });
            });
        </script>


        <script type="text/javascript">
            $(document).ready(function() {
                $('.tooltype').tooltip();
                $('.alert alert-danger alert-dismissible').add("button");  //função para criar caixas de texto ao passar o cursor do mouse.

                /* essas são responsáveis por fazer as mensagens flash desaparecerem suavemente após
                 * determinado período de tempo.
                 */
                $(".alert-success").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-success").alert('close');
                });
                $(".alert-danger").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
                $(".alert-warning").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
                $('.alert').addClass('animated fadeIn');
            });
        </script>          
    </body>
</html>


