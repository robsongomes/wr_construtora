<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?= (isset($pageTitle) && !empty($pageTitle)) ? $pageTitle : 'WR Construtora' ?></title>

        <!--Importando a fonte-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:700,100" rel="stylesheet" type="text/css">

        <!--Importando font-awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <!--importando boottrap-->
        <?php echo Asset::css('bootstrap.min.css'); ?>
        <?php echo Asset::css('bootstrap-theme.min.css'); ?>       

        <!--Animate.css -->        
        <?php echo Asset::css('animate.css'); ?>

        <!--Importando estilo do template-->        
        <?php echo Asset::css('template.css'); ?>

        <!--Especificos da página atual logon.php -->
        <?php echo Asset::css('logon.css'); ?>
    </head>
    <body>    
        <?php if(Session::get('message') != null): ?>            
            <div class="alert alert-<?php echo Session::get('message')->type ?>"><?php echo Session::get('message')->body ?></div>
            <?php Session::delete('message'); ?>
        <?php endif; ?>
        <div class="container">
            <div class="row" style="margin-top:70px">
                <div id="div-logon" class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
                    <form role="form" method="post" action="">                    
                        <fieldset>
                            <h2>Efetuar logon</h2>
                            <hr class="colorgraph">
                            <div class="form-group">
                                <input type="text" name="login" id="email" class="form-control input-lg" placeholder="Nome de usuário" required autofocus>
                            </div>
                            <div class="form-group">
                                <input type="password" name="senha" id="password" class="form-control input-lg" placeholder="Senha" required>
                            </div>                    
                            <hr class="colorgraph">
                            <div class="row">                        
                                <div class="col-xs-12 col-sm-12 col-md-12">                            
                                    <button type="submit" class="btn btn-lg btn-primary btn-block"><i class="fa fa-sign-in fa-lg"></i> Entrar</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>

        </div>

        <!--Importando javascript-->
        <?php echo Asset::js('jquery.min.js'); ?>
        <?php echo Asset::js('bootstrap.min.js'); ?>   

        <!--Especificos da página atual logon.php -->
        <?php echo Asset::js('logon.js'); ?>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.tooltype').tooltip();
                $('.alert alert-danger alert-dismissible').add("button");  //função para criar caixas de texto ao passar o cursor do mouse.

                /* essas são responsáveis por fazer as mensagens flash desaparecerem suavemente após
                 * determinado período de tempo.
                 */
                $(".alert-success").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-success").alert('close');
                });
                $(".alert-danger").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
                $(".alert-warning").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
                $('.alert').addClass('animated fadeIn');
            });
        </script>          
    </body>
</html>
