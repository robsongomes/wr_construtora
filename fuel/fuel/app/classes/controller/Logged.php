<?php

/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.7
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2015 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */

class Controller_Logged extends Controller {
    
    protected $user;    
    
    /**
     * The basic welcome message
     *
     * @access  public
     * @return  Response
     */
    public function __construct() {       
        if (Session::get('user') == NULL) {                         
            Session::set('message', self::getMessage('warning', 'Você precisa entrar no sistema para usar esta funcionalidade'));
            Response::redirect('welcome/index');
        }
        $this->user = Session::get('user');                                   
    }   
    
    public function action_logout() {        
        if(Session::get('user') != NULL) {
            Session::delete('user');
        }        
        return Response::forge(View::forge('logon/index'));
    } 

    public static function getMessage($type, $message) {
        $output = new \stdClass();
        $output->type = $type;
        $output->body = $message;
        return $output;
    }
    
    public static function cutText($name, $size) {
        $nameSize = strlen($name);
        if ($nameSize > $size) {
            return substr($name, 0, $size) . "...";
        }
        return $name;
    }

}
