<?php

/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.7
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2015 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
use Model\ProjetoManager;
use Model\DespesaManager;
use Model\UsuarioManager;

class Controller_Home extends Controller_Logged {

    /**
     * The basic welcome message
     *
     * @access  public
     * @return  Response
     */
    public function action_index() {
        $view = View::forge('home/index');   
        try {                      
            $projetos = ProjetoManager::getAll();                
            $view->set('projetos', $projetos);                        
            return Response::forge($view);
        } catch (Exception $ex) {
            Session::set('message', self::getMessage('danger', $ex->getMessage()));            
            return Response::forge($view);
        }
    }       
    
    public function action_projeto($idProjeto) {
        $view = View::forge('home/projeto'); 
        try {
            $projeto = ProjetoManager::getById($idProjeto);
            $despesas = DespesaManager::getAllByIdProjeto($idProjeto);  
            $valorTotal = 0;
            foreach ($despesas as $d) {
                $valorTotal += $d->valor;
            }
            $view->set('despesas', $despesas);
            $view->set('projeto', $projeto);
            $view->set('valorTotal', $valorTotal);
            return Response::forge($view);
        } catch (Exception $ex) {
            Session::set('message', self::getMessage('danger', $ex->getMessage()));            
            return Response::forge($view);
        }
    }
    
    public function action_cadastrarProjeto() {
        $view = View::forge('home/cadastrarProjeto');
        try {
            if (Input::post()) {
                $descricao = ((Input::post('descricao') != null) && !empty(Input::post('descricao'))) ? Input::post('descricao') : null;
                $nome = html_entity_decode(Input::post('nome'));
                $descricao = html_entity_decode($descricao);                               
                if (ProjetoManager::save($nome, $descricao, $this->user->id)) {
                    Session::set('message', self::getMessage('danger', 'Erro ao cadastrar projeto'));
                    return Response::redirect('home/index');
                } else {
                    Session::set('message', self::getMessage('success', 'Projeto cadastrado com sucesso'));
                    return Response::redirect('home/index');
                }
            } else {                
                return Response::forge($view);
            }
        } catch (Exception $ex) {
            Session::set('message', self::getMessage('danger', $ex->getMessage()));
            return Response::redirect('home/index');
        }
    }

    public function action_deletarProjeto($idProjeto) {
        try {
            if(ProjetoManager::delete($idProjeto)) {
                Session::set('message', self::getMessage('danger', 'Por favor, remova as despesas referentes à este projeto'));
                return Response::redirect('home/index');
            } else {
                Session::set('message', self::getMessage('success', 'Projeto removido com sucesso'));
                return Response::redirect('home/index');
            }             
        } catch (Exception $ex) {            
            Session::set('message', self::getMessage('danger', $ex->getMessage()));
            return Response::redirect('home/index');
        }
    }
    
    /* Despesas*/
    public function action_cadastrarDespesa($idProjeto) {
        try {
            if(Input::post()) {         
                
                $valor = Input::post('valor');
                $descricao = html_entity_decode(Input::post('descricao'), ENT_COMPAT, 'UTF-8');
                //$descricao = utf8_decode($descricao);
                                
                if(DespesaManager::save($descricao, $valor, $idProjeto, $this->user->id)) {
                    Session::set('message', self::getMessage('danger', 'Erro ao cadastrar despesa'));
                    return Response::redirect("home/projeto/$idProjeto");
                } else {
                    Session::set('message', self::getMessage('success', 'Despesa cadastrada com sucesso'));                    
                    return Response::redirect("home/projeto/$idProjeto");
                }                
            } else {          
                $view = View::forge('home/cadastrarDespesa');
                $view->set('idProjeto', $idProjeto);
                return Response::forge($view);
            }
        } catch (Exception $ex) {
           Session::set('message', self::getMessage('danger', $ex->getMessage()));
           return Response::redirect("home/projeto/$idProjeto");       
        }
    }
    
    public function action_deletarDespesa($idDespesa, $idProjeto) {
        try {
            if(DespesaManager::delete($idDespesa, $idProjeto, $this->user->id)) {
                Session::set('message', self::getMessage('danger', 'Erro ao remover despesa'));
                return Response::redirect("home/projeto/$idProjeto");
            } else {
                Session::set('message', self::getMessage('success', 'Despesa removida com sucesso'));                    
                return Response::redirect("home/projeto/$idProjeto");
            }              
        } catch (Exception $ex) {
            Session::set('message', self::getMessage('danger', $ex->getMessage()));
            return Response::redirect("home/projeto/$idProjeto");  
        }
    }
    
    public function action_editarDespesa($idDespesa, $idProjeto) {
        try {
            if(Input::post()) {                
                if(DespesaManager::update($idDespesa, $idProjeto, Input::post('descricao'), Input::post('valor'), $this->user->id)) {
                    Session::set('message', self::getMessage('danger', 'Erro ao atualizar despesa'));
                    return Response::redirect("home/projeto/$idProjeto");
                } else {
                    Session::set('message', self::getMessage('success', 'Despesa atualizada com sucesso'));                    
                    return Response::redirect("home/projeto/$idProjeto");
                }               
            }
            $view = View::forge('home/cadastrarDespesa');
            $despesa = DespesaManager::getById($idDespesa, $idProjeto);
            $view->set('idProjeto', $idProjeto);
            $view->set('despesa', $despesa);
            return Response::forge($view);
        } catch (Exception $ex) {
            Session::set('message', self::getMessage('danger', $ex->getMessage()));
            return Response::redirect("home/projeto/$idProjeto");    
        }
    }
    
    /*Usuários*/
    public function action_alterarSenha() {
        try {
            if(Input::post()) {                
                $senhaAtual = html_entity_decode(Input::post('senhaAtual'), ENT_COMPAT, 'UTF-8');                
                //$senhaAtual = utf8_decode(Input::post('senhaAtual'));
                $novaSenha = html_entity_decode(Input::post('novaSenha'), ENT_COMPAT, 'UTF-8');
                //$novaSenha = utf8_decode(Input::post('novaSenha'));                
                if(UsuarioManager::updateSenhaUsuario($senhaAtual, $novaSenha, $this->user->id)) {
                    Session::set('message', self::getMessage('danger', 'Erro ao alterar a senha'));                    
                } else {
                    Session::set('message', self::getMessage('success', 'Senha alterada com sucesso'));
                }
                return Response::redirect('home/index');                
            } else {
                Session::set('message', self::getMessage('danger', 'Dados enviados incorretamente'));    
                return Response::redirect('home/index');       
            }
        } catch (Exception $ex) {
            Session::set('message', self::getMessage('danger', $ex->getMessage()));
            return Response::redirect('home/index');       
        }
    }
    
    public static function getMessage($type, $message) {
        $output = new \stdClass();
        $output->type = $type;
        $output->body = $message;
        return $output;
    }

}
