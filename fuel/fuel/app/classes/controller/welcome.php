<?php

/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.7
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2015 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
use Model\UsuarioManager;

class Controller_Welcome extends Controller {

    /**
     * The basic welcome message
     *
     * @access  public
     * @return  Response
     */
    public function action_index() {
        try {
            if ((Session::get('user') != NULL)) {
                Response::redirect('home/index');
            }
            if (Input::post()) {
                $login = Input::post('login');
                $senha = Input::post('senha');
                $user = UsuarioManager::getByLoginAndSenha($login, $senha);
                if(!$user) {                    
                    $view = View::forge('logon/index');                                
                    Session::set('message', self::getMessage('danger', 'Nome de usuário ou senha incorretos'));
                    return Response::forge($view);          
                } else {
                    Session::set('user', $user);
                    //$test = Session::get('usuario');
                    //print_r($test);
                    //Session::delete('usuario');
                    //$test = Session::get('usuario');
                    //echo 'sddsadsa   ';
                    //var_dump($test);exit;
                    Response::redirect('home/index');
                }
            } else {                
                return Response::forge(View::forge('logon/index'));
            }
        } catch (Exception $ex) {
            //create the view
            $view = View::forge('logon/index');                        
            Session::set('message', self::getMessage('danger', $ex->getMessage()));
            return Response::forge($view);
        }
    }        

    /**
     * A typical "Hello, Bob!" type example.  This uses a Presenter to
     * show how to use them.
     *
     * @access  public
     * @return  Response
     */
    public function action_hello() {
        return Response::forge(Presenter::forge('welcome/hello'));
    }

    /**
     * The 404 action for the application.
     *
     * @access  public
     * @return  Response
     */
    public function action_404() {
        return Response::forge(Presenter::forge('welcome/404'), 404);
    }

    public static function getMessage($type, $message) {
        $output = new \stdClass();
        $output->type = $type;
        $output->body = $message;
        return $output;
    }
    

}
