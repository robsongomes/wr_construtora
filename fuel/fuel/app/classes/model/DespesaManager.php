<?php

namespace Model;
use Model\UsuarioManager;

class DespesaManager extends \Model {
    
    public static function getAllByIdProjeto($idProjeto) {
        try {
            $sql = "SELECT * FROM wr_construtora.despesa WHERE idProjeto = ? ORDER BY dataDoCadastro DESC";
            $query = UsuarioManager::getConection()->prepare($sql);
            $query->execute(array($idProjeto));
            $dados = $query->fetchAll(\PDO::FETCH_ASSOC);            
            return UsuarioManager::getStd($dados);          
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    public static function getById($idDespesa, $idProjeto) {
        try {
            $sql = "SELECT * FROM wr_construtora.despesa WHERE id = ? AND idProjeto = ? LIMIT 1";
            $query = UsuarioManager::getConection()->prepare($sql);
            $query->execute(array($idDespesa, $idProjeto));
            $dados = $query->fetchAll(\PDO::FETCH_ASSOC);
            $dados = UsuarioManager::getStd($dados);
            if(!empty($dados)) {
                return $dados[0];
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    public static function save($descricao, $valor, $idProjeto, $idUsuario) {
       try {
           $sql = "INSERT INTO wr_construtora.despesa (descricao, valor, idProjeto, idUsuarioCadastro, dataDoCadastro)
                   VALUES(?, ?, ?, ?, ?)";
           $query = UsuarioManager::getConection()->prepare($sql);
           $query->execute(array($descricao, $valor, $idProjeto, $idUsuario, date('Y-m-d H:i:s')));
           $cont = $query->rowCount();
           if($cont == 0) {
               return true;
           }
       } catch (Exception $ex) {
          throw new Exception($ex->getMessage());
       } 
    }
    
    public static function saveDespesaRemovida($descricao, $valor, $idProjeto, $idUsuario, $dataDoCadastro) {
        try {
            $sql = "INSERT INTO wr_construtora.despesa_removida (descricao, valor, idProjeto, idUsuarioRemoveu, dataDoCadastro, dataDaRemocao)
                   VALUES(?, ?, ?, ?, ?, ?)";
           $query = UsuarioManager::getConection()->prepare($sql);
           $query->execute(array($descricao, $valor, $idProjeto, $idUsuario, $dataDoCadastro, date('Y-m-d H:i:s')));
           $cont = $query->rowCount();
           if($cont == 0) {
               return false;
           }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    public static function delete($idDespesa, $idProjeto, $idUsuario) {
       try {
           /*Salva a despesa removida na tabela despesa_removida para controle de alterações*/
           $despesa = self::getById($idDespesa, $idProjeto);           
           self::saveDespesaRemovida($despesa->descricao, $despesa->valor, $despesa->idProjeto, $idUsuario, $despesa->dataDoCadastro);
           
           $sql = "DELETE FROM wr_construtora.despesa WHERE id = ? AND idProjeto = ?";
           $query = UsuarioManager::getConection()->prepare($sql);
           $query->execute(array($idDespesa, $idProjeto));
           $cont = $query->rowCount();
           if($cont == 0) {
               return true;
           }
       } catch (Exception $ex) {
          throw new Exception($ex->getMessage());
       }
    } 
    
    public static function update($idDespesa, $idProjeto, $descricao, $valor, $idUsuario) {
        try {
            $db = UsuarioManager::getConection();
            $db->beginTransaction();
            $sql = "UPDATE wr_construtora.despesa SET descricao = ?,
                    valor = ?, idUsuarioCadastro = ?, dataDoCadastro = ?
                    WHERE id = ? AND idProjeto = ?";
            $query = $db->prepare($sql);
            $query->execute(array($descricao, $valor, $idUsuario, date('Y-m-d H:i:s'), $idDespesa, $idProjeto));
            $cont = $query->rowCount();
            if($cont == 1) {
                $db->commit();
            } else {
                $db->rollBack();
                return true;
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }        
}
