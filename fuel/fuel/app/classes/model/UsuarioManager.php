<?php

namespace Model;

class UsuarioManager extends \Model{
    
    public static function getByLoginAndSenha($login, $senha) {                       
        try {
            $sql = "SELECT * FROM wr_construtora.usuario WHERE login = ? AND senha = ? LIMIT 1";
            $query = self::getConection()->prepare($sql);
            $query->execute(array($login, $senha));
            $dados = $query->fetchAll(\PDO::FETCH_ASSOC);                        
            $dados = self::getStd($dados);            
            if($dados) {
                return $dados[0];
            } else {
                return false;
            }
        } catch (Exception $ex) {            
            throw new Exception($ex->getMessage());
        }
    }
    
    public static function updateSenhaUsuario($senhaAtual, $novaSenha, $idUsuario) {
        try {
            $sql = "UPDATE wr_construtora.usuario SET senha = ?, dataDoCadastro = ? WHERE senha = ? AND id = ?";
            $query = self::getConection()->prepare($sql);
            $query->execute(array($novaSenha, date('Y-m-d H:i:s'), $senhaAtual, $idUsuario));
            $cont = $query->rowCount();
            if($cont != 1) {                
                return true;
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    public static function getConection($db = 'wr_construtora') {
        try {
            $host = "localhost";
            $nameBanco = $db;
            $nameUser = "wr";
            $passwordUser = "AcDxYcX9bLTe28";
            $pdo = new \PDO("mysql:server=$host;Database=$nameBanco;charset=utf8", $nameUser, $passwordUser);            
            return $pdo;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    } 

    public static function getStd($dados) {
        $results = array();
        foreach($dados as $a) {
            $object = new \stdClass();
            foreach ($a as $field => $value)
                $object->{$field} = $value;
            $results[] = $object;
        }
        return $results;
    }
}
