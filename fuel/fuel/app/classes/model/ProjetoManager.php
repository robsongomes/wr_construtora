<?php

namespace Model;
use Model\UsuarioManager;

class ProjetoManager extends \Model {
    
    public static function getAll() {
        try {
            $sql = "SELECT * FROM wr_construtora.projeto ORDER BY dataDoCadastro DESC";
            $query = UsuarioManager::getConection()->prepare($sql);
            $query->execute();
            $dados = $query->fetchAll(\PDO::FETCH_ASSOC);            
            return UsuarioManager::getStd($dados);     
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    public static function getById($idProjeto) {
        try {
            $sql = "SELECT * FROM wr_construtora.projeto WHERE id = ? LIMIT 1";
            $query = UsuarioManager::getConection()->prepare($sql);
            $query->execute(array($idProjeto));
            $dados[] = $query->fetch(\PDO::FETCH_ASSOC);             
            if(!empty($dados)) {
                return (UsuarioManager::getStd($dados)['0']);
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }    
    }
    
    public static function save($nome, $descricao, $idUsuario) {       
        try {
            $sql = "INSERT INTO wr_construtora.projeto (nome, descricao, dataDoCadastro, status, idUsuarioCadastro)
                    VALUES(?, ?, ?, ?, ?)";
            $query = UsuarioManager::getConection()->prepare($sql);
            $query->execute(array($nome, $descricao, date('Y-m-d H:i:s'), 1, $idUsuario));
            if($query->rowCount() == 0) {                
                return true;
            }
        } catch (Exception $ex) {            
            throw new Exception($ex->getMessage());
        }
    }
    
    public static function delete($idProjeto) {
        try {
            $sql = "SELECT COUNT(*) Total FROM wr_construtora.despesa WHERE idProjeto = ?";
            $query = UsuarioManager::getConection()->prepare($sql);
            $query->execute(array($idProjeto));
            $cont = $query->fetch(\PDO::FETCH_ASSOC)['Total'];            
            if($cont == 0) {
                $sql2= "DELETE FROM wr_construtora.projeto WHERE id = ?";
                $query2 = UsuarioManager::getConection()->prepare($sql2);
                $query2->execute(array($idProjeto));
                $cont2 = $query2->rowCount();
                if($cont2 != 1) {
                   return true;
                }
            } else {                
                return true;
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
}
