<?php

class UsuarioManager extends Manager {
    
    public static function getByLoginAndSenha($login, $senha) {
        try {            
            $sql = "SELECT * FROM wr_construtora.usuario WHERE login = ? AND senha = ? LIMIT 1";
            $query = self::getConection()->prepare($sql);
            $query->execute(array($login, $senha));
            $dados = $query->fetchAll(PDO::FETCH_ASSOC);            
            $dados = self::getStd($dados);
            if($dados) {
                return $dados[0];
            } else {
                throw new Exception('Nome de usuário ou senha incorretos');
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    public static function updateSenhaUsuario($senhaAtual, $novaSenha, $idUsuario) {
        try {
            $sql = "UPDATE wr_construtora.usuario SET senha = ?, dataDoCadastro = ? WHERE senha = ? AND id = ?";
            $query = self::getConection()->prepare($sql);
            $query->execute(array($novaSenha, date('Y-m-d H:i:s'), $senhaAtual, $idUsuario));
            $cont = $query->rowCount();
            if($cont != 1) {
                throw new Exception('Senha atual inválida');
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
}
