<style>
     
</style>

<nav id="navbarPrincipal" class="navbar navbar-default navbar-static">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="~/home/index">WR Const.</a>
        </div>
        <div class="collapse navbar-collapse bs-example-js-navbar-collapse">          
            <ul class="nav navbar-nav navbar-right hidden-xs hidden-sm">
                <li id="fat-menu" class="dropdown">
                    <a id="drop3" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                        <i class="fa fa-bars fa-lg"></i> 
                    </a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="~/home/cadastrarProjeto"><i class="fa fa-plus"></i> Novo Projeto</a></li>
                        <li class="removerProjeto" role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-trash"></i> Remover Projeto</a></li>                        
                        <li role="presentation" class="alterarSenha"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-key"></i> Alterar Senha</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="~/logon/logout"><i class="fa fa-sign-out"></i> Sair</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right hidden-md hidden-lg">                                    
                <li role="presentation"><a role="menuitem" tabindex="-1" href="~/home/cadastrarProjeto"><i class="fa fa-plus"></i> Novo Projeto</a></li>                                                
                <li class="removerProjeto" role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-trash"></i> Remover Projeto</a></li>    
                <li role="presentation" class="alterarSenha"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-key"></i> Alterar Senha</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="~/logon/logout"><i class="fa fa-sign-out"></i> Sair</a></li>                    
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">       
    <div class="row">              
        <?php if(isset($projetos) && !empty($projetos)): ?>
            <?php foreach ($projetos as $key => $p): ?>                
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">                                                            
                    <a href="~/home/projeto/<?= $p->id ?>" id="<?= $p->id ?>" value="<?= $p->id ?>" class="thumbnail animated fadeInDown card-project">                                                
                        <p><?= (isset($p->nome) && !empty($p->nome))? Utils::cutText(utf8_encode($p->nome), 25):'--' ?></p>
                    </a>                           
                </div>                
            <?php endforeach; ?>
        <?php else: ?>            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Nenhum projeto cadastrado ainda.</h2>
            </div>              
        <?php endif; ?>    
    </div>      
</div>

<!--Modal confirmação de remoção de estoque -->
<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px " >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="closeInfo" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Excluir Projeto</h4>
            </div>

            <div class="modal-body" id="divExcluir">
                <p>Tem certeza que deseja excluir este projeto?</p>
            </div>         
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i class="fa fa-ban"></i> Não</button>
                <a href="#"  id="btnExcluir" class="btn btn-default btn-xs"><i class="fa fa-check"></i> Sim</a>
            </div>            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!--Modal alterar senha -->
<div class="row">
    <div class="modal fade col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2" id="modalAlterarSenha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="row">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                        
                        <h4>Alterar senha</h4>
                    </div>
                    <form id="formAlterarSenha" name="formAlterarSenha" method="post" action="~/home/alterarSenha">
                        <div class="modal-body">                        
                            <div class="form-group">
                                <label for="senhaAtual">Senha Atual*</label>
                                <input type="password" id="senhaAtual" name="senhaAtual" maxlength="50" class="form-control" placeholder="Digite a senha atual" required autofocus>
                            </div>                            
                            <div class="form-group">
                                <label for="novaSenha">Nova Senha*</label>
                                <input type="password" id="novaSenha" name="novaSenha" maxlength="50" class="form-control" placeholder="Digite a nova senha" required>
                            </div>                                                    
                        </div>         
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
                            <button type="submit" id="btnAlterarSenha" class="btn btn-default btn-xs"><i class="fa fa-check"></i> Salvar</a>                        
                        </div> 
                    </form>    
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>    
    </div>
</div> 
<script type="text/javascript">
    $(document).ready(function() { 
        
        var color = true;
        $('.removerProjeto').click(function() {           
            if(color) {
                $('.card-project').each(function() {
                    var id = $(this).attr('id');
                    $(this).attr('href', '~/home/deletarProjeto/'+id+'');
                });                  
                color = false;
                $('.card-project').css("background", "#faebcc");
                $('.card-project').css("border-left", "5px solid #af0000");
            } else {
                $('.card-project').each(function() {
                    var id = $(this).attr('id');
                    $(this).attr('href', '~/home/projeto/'+id+'');
                });
                color = true;
                $('.card-project').css("background", "#fff");
                $('.card-project').css("border-left", "5px solid #3c763d");
            }            
        });
        
        $('.deleteItem').click(function() {
            var id = $(this).attr('id');          
            $('#btnExcluir').attr('href', '~/home/deletarProjeto/'+id+'');
            $('#modalExcluir').modal('show');
        });
        
        $('.alterarSenha').click(function() {
           $('#modalAlterarSenha').modal('show');
        });
                                
    });
       
</script>