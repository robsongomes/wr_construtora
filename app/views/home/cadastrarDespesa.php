
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
        <h3 class="titulo"><?= (isset($despesa) && !empty($despesa))? 'Despesa "'.Utils::cutText(utf8_encode($despesa->descricao), 22).'"' : 'Nova Despesa' ?></h3>
        <hr>
        <form id="formCadastrar" name="formCadastrar" method="post" action="~/home/<?= (isset($despesa) && !empty($despesa))? "editarDespesa/$despesa->id/$idProjeto":"cadastrarDespesa/$idProjeto" ?>">            
            <div class="form-group">
                <label for="descricao">Descrição*</label>
                <input type="text" maxlength="500" class="form-control" id="descricao" name="descricao" value="<?= (isset($despesa->descricao) && !empty($despesa->descricao))? $despesa->descricao:'' ?>" placeholder="Digite a descrição" required autofocus>
            </div>
            <div class="form-group">
                <label for="valor">Valor*</label>
                <input type="number" step="any" min="1" class="form-control" id="valor" name="valor" placeholder="Digite o valor da despesa" value="<?= (isset($despesa->valor) && !empty($despesa->valor))? $despesa->valor:'' ?>" required>
            </div>                                  
            <hr>             
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Salvar</button>
            <a href="~/home/projeto/<?= $idProjeto ?>"  class="btn btn-danger pull-right" style="margin-right: 8px;"><i class="fa fa-ban"></i> Cancelar</a>
        </form>  
    </div>
</div>
<script>
    $(document).ready(function() {
       
    });        
</script>