
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
        <h2 class="titulo">Novo Projeto</h2>
        <form id="formCadastrar" name="formCadastrar" method="post" action="~/home/<?= (isset($projeto) && !empty($projeto))? "editarProjeto/$projeto->id":'cadastrarProjeto' ?>">
            <div class="form-group">
                <label for="nome">Nome*</label>
                <input type="text" class="form-control" maxlength="200" id="nome" name="nome" value="<?= (isset($projeto->nome) && !empty($projeto->nome))? $projeto->nome:'' ?>" placeholder="Digite o nome" required autofocus>
            </div>
            <div class="form-group">
                <label for="descricao">Descrição</label>
                <textarea  class="form-control" maxlength="500" id="descricao" name="descricao" value="<?= (isset($projeto->descricao) && !empty($projeto->descricao))? $projeto->descricao:'' ?>" placeholder="Digite uma descrição (Opcional)"></textarea>
            </div>                                  
            <hr>             
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Salvar</button>
            <a href="~/home/index"  class="btn btn-danger pull-right" style="margin-right: 8px;"><i class="fa fa-ban"></i> Cancelar</a>
        </form>  
    </div>
</div>
<script>
    $(document).ready(function() {
       
    });        
</script>