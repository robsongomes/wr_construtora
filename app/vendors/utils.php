<?php

class Utils {

    public static function cutText($name, $size) {
        $nameSize = strlen($name);
        if ($nameSize > $size) {
            return substr($name, 0, $size) . "...";
        }
        return $name;
    }

}
