<?php

class Manager {

    public static function getConection($db = 'wr_construtora') {
        $host = "localhost";
        $nameBanco = $db;
        $nameUser = "root";
        $passwordUser = "1234";
        $InstanciaPDO = new PDO("mysql:server=$host;Database=$nameBanco", $nameUser, $passwordUser);
        return $InstanciaPDO;
    }

    public static function getStd($dados) {
        $results = array();
        foreach($dados as $a) {
            $object = new stdClass();
            foreach ($a as $field => $value)
                $object->{$field} = $value;
            $results[] = $object;
        }
        return $results;
    }

}
