<?php

class LoggedController extends Controller {

    protected $user;
    public $page_title = "Wr Construtora";
    
    public function __construct() {
        if (!Auth::user() || !Auth::isLogged()) {
            $this->_flash('alert alert-warning', 'Você precisa entrar no sistema para usar esta funcionalidade');
            return $this->_redirect('~/logon/index');
        }
        $this->user = Auth::user();            
        $this->_set('page_title', $this->page_title);
        $this->_set('usuario', $this->user);                
    }
    
    public function limparDadosUsuario() {
        $this->user = null;
        $this->_set('usuario', null);
    }
    
    public static function getMessage($type, $message) {
        $output = new \stdClass();
        $output->type = $type;
        $output->body = $message;
        return $output;
    }
          
}
?>
