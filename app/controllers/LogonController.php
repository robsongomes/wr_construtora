<?php
class LogonController extends Controller
{
//    public function beforeRender() {        
//        $this->Template->setMaster('template_interno');
//    }
    
    public function index() {
        try {              
            if(Auth::isLogged()) {                  
                return $this->_redirect('~/home/index');
            } else {                
                return $this->_view();                
            }
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/logon/index');
        }
    }    
    public function logon() {                          
        try {
            if (Auth::isLogged()) {
                return $this->_redirect('~/home/index');
            }
            if(is_post) {                    
                $post = $this->_data();                     
                $usuario = UsuarioManager::getByLoginAndSenha($post->login, $post->senha);                
                if(!empty($usuario)) {                    
                    Auth::set('Administrador');                
                    Auth::login($usuario);                    
                    return $this->_redirect('~/home/index');
                } else {                               
                    $this->_flash('alert alert-warning', 'Nome de usuário ou senha incorretos');
                    return $this->_redirect('~/logon/index');
                }                
            } else {                    
                return $this->_view();
            }            
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/logon/index');
        }
    }
    
    public function logout() {        
        if(Auth::isLogged()) {
            Auth::remove('admin');            
            Auth::remove('normal');
            Auth::logout();            
        }
        //LoggedController::limparDadosUsuario();
        return $this->_redirect('~/logon/index');
    }   
}