<?php

class HomeController extends LoggedController {
    
    public function index() {
        try {
            $projetos = ProjetoManager::getAll();            
            $this->_set('projetos', $projetos);
            return $this->_view();            
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_view();    
        }
    }
    
    public function projeto($idProjeto) {
        try {            
            $projeto = ProjetoManager::getById($idProjeto);
            $despesas = DespesaManager::getAllByIdProjeto($idProjeto);  
            $valorTotal = 0;
            foreach ($despesas as $d) {
                $valorTotal += $d->valor;
            }
            $this->_set('despesas', $despesas);
            $this->_set('projeto', $projeto);
            $this->_set('valorTotal', $valorTotal);
            return $this->_view();
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/home/index'); 
        }
    }
    
    public function cadastrarProjeto() {
        try {
            if(is_post) {
                $dados = $this->_data();                
                $dados->descricao = (isset($dados->descricao) && !empty($dados->descricao))? $dados->descricao:null;
                $dados->nome = html_entity_decode($dados->nome);                
                $dados->descricao = html_entity_decode($dados->descricao);
                $dados->nome = utf8_decode($dados->nome);                                
                $dados->descricao = utf8_decode($dados->descricao);                 
                ProjetoManager::save($dados->nome, $dados->descricao, $this->user->id);                
                $this->_flash('alert alert-success', 'Projeto cadastrado com sucesso');
                return $this->_redirect('~/home/index');  
            } else {
                return $this->_view();
            }
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/home/index');  
        }
    }
    
    public function deletarProjeto($idProjeto) {
        try {
            ProjetoManager::delete($idProjeto);
            $this->_flash('alert alert-success', 'Projeto removido com sucesso');
            return $this->_redirect('~/home/index');  
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/home/index'); 
        }
    }
    
    /* Despesas*/
    public function cadastrarDespesa($idProjeto) {
        try {
            if(is_post) {                
                $dados = $this->_data();
                $dados->descricao = html_entity_decode($dados->descricao, ENT_COMPAT, 'UTF-8');
                $dados->descricao = utf8_decode($dados->descricao);
                DespesaManager::save($dados->descricao, $dados->valor, $idProjeto, $this->user->id);
                $this->_flash('alert alert-success', 'Despesa cadastrada com sucesso');
                return $this->_redirect("~/home/projeto/$idProjeto");  
            } else {                
                $this->_set('idProjeto', $idProjeto);
                return $this->_view();
            }
        } catch (Exception $ex) {
           $this->_flash('alert alert-danger', $ex->getMessage());
           return $this->_redirect("~/home/projeto/$idProjeto");       
        }
    }
    
    public function deletarDespesa($idDespesa, $idProjeto) {
        try {
            DespesaManager::delete($idDespesa, $idProjeto, $this->user->id);
            $this->_flash('alert alert-success', 'Despesa removida com sucesso'); 
            return $this->_redirect("~/home/projeto/$idProjeto");  
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect("~/home/projeto/$idProjeto");   
        }
    }
    
    public function editarDespesa($idDespesa, $idProjeto) {
        try {
            if(is_post) {
                $dados = $this->_data();
                DespesaManager::update($idDespesa, $idProjeto, $dados->descricao, $dados->valor, $this->user->id);
                $this->_flash('alert alert-success', 'Despesa atualizada com sucesso');
                return $this->_redirect("~/home/projeto/$idProjeto"); 
            }
            $despesa = DespesaManager::getById($idDespesa, $idProjeto);
            $this->_set('despesa', $despesa);
            return $this->_view('cadastrarDespesa');
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect("~/home/projeto/$idProjeto");   
        }
    }
    
    /*Usuários*/
    public function alterarSenha() {
        try {
            if(is_post) {
                $dados = $this->_data();
                $dados->senhaAtual = html_entity_decode($dados->senhaAtual, ENT_COMPAT, 'UTF-8');                
                $dados->senhaAtual = utf8_decode($dados->senhaAtual);
                $dados->novaSenha = html_entity_decode($dados->novaSenha, ENT_COMPAT, 'UTF-8');
                $dados->novaSenha = utf8_decode($dados->novaSenha);                
                UsuarioManager::updateSenhaUsuario($dados->senhaAtual, $dados->novaSenha, $this->user->id);
                $this->_flash('alert alert-success', 'Senha alterada com sucesso'); 
                return $this->_redirect("~/home/index");
            } else {
                throw new Exception('Dados enviados incorretamente');
            }
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect("~/home/index");
        }
    }
}

